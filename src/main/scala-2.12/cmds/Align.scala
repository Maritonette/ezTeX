package cmds

class Align extends Exec {
  override def ID: String = "math"


  override def handle(input: String): String = {
    var in = input
    val dfracPattern = """\(([^\)]+)\)/\(([^\)]+)\)""".r
    in = dfracPattern.replaceAllIn(in, "dfrac{$1}{$2}")
    in = in.replace("dfrac", "\\dfrac").replace("$\\Omega$", "\\Omega")
    val cmds = commands(in)
    var ret = "\\begin{align}\n"
    cmds.foreach(m => {
      ret += "\t" + m + "\\\\\n"
    })
    ret += "\\end{align}"
    ret
  }
}
