package cmds

class Input extends Exec {
  override def ID: String = "inc"

  override def handle(input: String): String = "\\include{"+input+"}"
}
