package cmds

import utils.Escaper

class Text extends Exec {
  override def ID: String = "text"

  override def handle(input: String): String = {
    var myInput = input.trim
    myInput = Escaper.escapeTilde(myInput)
    myInput = Escaper.escapeAnd(myInput)
    myInput = Escaper.escapeHash(myInput)
    myInput = Escaper.deleteTabs(myInput)
    myInput = Escaper.escapeDollar(myInput)
    myInput = Escaper.escapePercent(myInput)
    myInput = Escaper.replaceWithMath(myInput)
    myInput = Escaper.escapeCircilium(myInput)
    myInput
  }
}
