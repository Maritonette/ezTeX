package cmds

import utils.Escaper

class Latex extends Exec {
  override def ID: String = "latex"

  override def handle(input: String): String = {
    var myInput = input.trim
    Escaper.deleteTabs(myInput)
  }
}