package cmds

abstract class Exec {
  /**
    * Die ID der Klasse
    * @return ID
    */
  def ID:String
  /**
    * Transformiert den Input in LaTeX-Code
    * @return ... Latex-Code
    */
  def handle(input : String):String

  def commands(in : String) : Array[String] = {
    /*
     * x, y,z
     * Array("x", "y", "z")
     */
    in.split(""",(\s+)?""").map(_.trim)
  }
}
