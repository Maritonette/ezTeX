package cmds

class Section extends Exec {
  override def ID:String = "sec"

  override def handle(input: String): String = {
    "\\section{" + input + "}"
  }
}
class SubSection extends Exec {
  override def ID: String = "subsec"

  override def handle(input: String): String = {
    "\\subsection{" + input + "}"
  }
}

class SubSubSection extends Exec {
  override def ID: String = "subsubsec"

  override def handle(input: String): String = "\\subsubsection{" + input + "}"
}
