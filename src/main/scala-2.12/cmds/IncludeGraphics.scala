package cmds

class IncludeGraphics extends Exec {
  def prefix = "img"

  override def ID: String = "img"

  override def handle(input: String): String = {
    val cmd = commands(input)
    if (cmd.length==2){
      "\\begin{figure}[H]\n" +
      "\t\\centering\n" +
      "\t\\includegraphics[width=0.8\\textwidth]{img/" + commands(input).apply(0) + "}\n" +
      "\t\\label{fig:" + commands(input).apply(0) + "}\n" +
      "\t\\caption{" + commands(input).apply(1) + "}\n" +
      "\\end{figure}"
    } else {
      throw new Exception("Wrong amount of Arguments!")
    }
  }
}
