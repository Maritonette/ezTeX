package utils

import java.io._

import cmds._

import scala.Console
import scala.util.matching.Regex

object Translator {

  def translateFile(file : File, outputFile : File): Boolean = {
    val fileInput = IO.getInput(file)
    PatternHandler.translateString(fileInput, outputFile)
  }
}
