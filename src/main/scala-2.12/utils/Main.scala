package utils

import java.io.File

object Main {
  val DEVMODE: Boolean = true

  def main(args: Array[String]): Unit = {
    println("eztex translator")

    var fileToCompile:File = new File("")

    if (DEVMODE) {
      println("DEVMODE")
      fileToCompile= new File("./input.eztex")
    } else {
      if (args.nonEmpty) {
        fileToCompile = new File(args.head)
      } else {
        println("There are no parameter! Please give me a file!")
      }
    }

    if (fileToCompile.exists()){
      if (Translator.translateFile(fileToCompile, new File(fileToCompile.getPath.replace(".eztex", ".tex")))){
        println(s"File ${fileToCompile.getCanonicalPath} translated!")
      } else {
        println(s"File ${fileToCompile.getCanonicalPath} could not be translated!")
      }
    } else {
      println(s"File ${fileToCompile.getCanonicalPath} does not exist!")
    }
  }
}
