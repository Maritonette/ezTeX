package utils

import java.io.File

import cmds._

import scala.util.matching.Regex

object PatternHandler {

  private val infoPattern  = """(\w+)\s?\{([^\}]+)\}""".r
  private val latexPattern = """(\\\w+\s*?)\{(\w*)\}""".r
  val openBrace = "::OPENBRACE::"
  val closedBrace = "::CLOSEDBRACE::"
  private val handler: Array[Exec] = Array(
    new Section,
    new SubSection,
    new SubSubSection,
    new IncludeGraphics,
    new Input,
    new Align,
    new Text
  )

  private def transformInput(input: String):String = {
    var i = input.replace("Ohm", "$\\Omega$")
    i = latexPattern.replaceAllIn(i, "$1"+openBrace+"$2"+closedBrace)
    i
  }

  @throws(classOf[Exception])
  private def checkInput(input: String): Unit = {
    val checkInput = infoPattern.replaceAllIn(input, "").trim
    if (checkInput != ""){
      throw new Exception(s"${Console.RED}Syntax Error: $checkInput${Console.RESET}")
    }
  }

  private def handle(input: Regex.MatchIterator): String = {
    var ret = ""
    input.foreach(s => {
      val id = infoPattern.findAllIn(s).group(1)
      handler.foreach(h => {
        if (h.ID.equals(id)) {
          try {
            ret += h.handle(infoPattern.findAllIn(s).group(2)) + "\n"
          } catch {
            case e: Exception =>
              throw new Exception(s"${Console.RED}Error in Line ${input.indexOf(s) + 1}  |  + ${e.getLocalizedMessage}${Console.RESET}")
          }
        }
      })
    })
    transformInput(ret)
  }

  def translateString(string: String, outputFile : File): Boolean = {
    val input = infoPattern.findAllIn(string)
    try {
      checkInput(string)
      val output = handle(input)
      IO.writeOutput(output, outputFile)
      true
    } catch {
      case e:Exception =>
        println(e.getMessage)
        false
    }
  }
}
