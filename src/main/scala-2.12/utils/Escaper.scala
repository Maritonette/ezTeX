package utils

// TODO: Settings File

object Escaper {
  def escapeDollar(string : String): String = {
    string.replaceAll("\\$", "\\\\\\$")
  }

  def escapePercent(string : String): String = {
    string.replaceAll("\\%", "\\\\%")
    
  }

  def escapeAnd(string : String): String = {
    string.replaceAll("\\&", "\\\\&")
    
  }

  def escapeHash(string : String): String = {
    string.replaceAll("\\#", "\\\\#")
    
  }

  def escapeTilde(string : String): String = {
    string.replaceAll("\\~", "\\\\textasciitilde")
    
  }

  def escapeUnderscore(string : String): String = {
    string.replaceAll("\\_", "\\\\_")
    
  }

  def escapeCurlyBrackets(string : String): String = {
    string.replaceAll("\\{", "\\\\{").replace("\\}", "\\\\}")
    
  }

  def escapeCircilium(string : String): String = {
    string.replaceAll("\\^", "\\\\textasciicircum")
    
  }

  def escapeBackslash(string : String): String = {
    string.replaceAll("\\\\", "\\\\backslash")
    
  }

  def replaceWithMath(string : String): String = {
    val regexPattern = """([a-zA-z0-9]+)_([a-zA-z0-9]+)""".r
    regexPattern.replaceAllIn(string, "\\$$1_{$2}\\$")
  }

  def deleteTabs(string : String): String = {
    string.replaceAll("(?m)^(\\t|\\s{4}|\\s{2})", "")
    
  }
}
