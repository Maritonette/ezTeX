package utils

import java.io._

object IO {

  def getProjectSettings(): Unit ={

  }

  def writeOutput(out : String, outputFile : File): Unit = {
    var o = out.replace(PatternHandler.openBrace, "{")
    o = o.replace(PatternHandler.closedBrace, "}")
    val bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF8"))
    bw.write(o)
    bw.flush()
    bw.close()
  }

  def getInput(file: File): String = {
    var ret: String = ""
    val br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"))
    br.lines().forEach(s => {
      ret += s + "\n"
    })
    br.close()
    ret = ret.replaceAll("#.*\n", "\n")
    ret
  }

}
