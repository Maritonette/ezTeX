package load

import java.io.File

import pluginShell.pluginsAPI.{Handler, Plugins}
import utils.Translator

import scala.collection.mutable.ListBuffer

class Plugin extends Plugins{
  override val NAME: String = "eztex"
  override val VERSION: String = "2018.03"

  override def init(): ListBuffer[Handler] = {
    val listBuffer : ListBuffer[Handler] = ListBuffer()
    listBuffer += new Handler {
      override def handle(dir: File, args: Array[String]): Boolean = {
        if (args.length < 2){
          return false
        }
        Translator.translateFile(new File(dir.getCanonicalPath+args(0)), new File(dir.getCanonicalPath+args(1)))
      }

      override val NAME: Array[String] = Array("eztex")
    }
    listBuffer
  }
}
