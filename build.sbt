name := "LaTeX"

version := "2018.03-SNAOSHOT"

scalaVersion := "2.12.5"

val online = false

if (online){
  resolvers += "dragoncoder" at "https://gitlab.com/DragonCoder/maven-repo/raw/master"
} else {
  resolvers += Resolver.mavenLocal
}

libraryDependencies += "pluginShell" % "pluginapi_2.12" % "2018.03+"
